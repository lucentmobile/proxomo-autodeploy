
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var spawn   = require('child_process').spawn;
var exec = require('child_process').exec;


var settings = require('./settings.json');

var app = express();

// all environments
app.set('port', process.env.PORT || 5000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);


http.createServer(app).listen(app.get('port'), function(){
 console.log('Express server listening on port ' + app.get('port'));
});

app.post('/postrecieve', function(req, res){
    console.log(req.body.push.changes[0].name)
    var currentBranch = req.body.push.changes[0].name;
    var settingsBranch =settings.repo.branch;
    if(currentBranch == settingsBranch)
    {

    	var scriptToLaunch = "relaunch-" + currentBranch + ".sh"
    	executeUpdateScript(scriptToLaunch);
    }
    else
    {
       //Do nothing because this is not the correct branch
    }
    res.send("ok");

});


function executeUpdateScript(scriptName){
   //look at this
    var now = new Date();
    now.toDateString();

    exec('~/./' + scriptName,{maxBuffer: 1024 * 500},
        function (error, stdout, stderr) {
            console.log('stdout: \n' + 'Deploy time : ' + now +
                '\n' +
                '' + stdout + '\n\n');
            console.log('stderr: ' +
                '' +
                '' + stderr);
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });


}



